﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Match
    {
        DateTime date;
        Team host, guest;
        Judge judge;
        List<Goal> goals;

        public string Date { get => date.ToString("yyyy/MM/dd"); }
        public Judge Judge { get => judge; }
        public Team Host { get => host; }
        public Team Guest { get => guest; }

        private int hostResult;
        private int guestResult;

        public Match (DateTime date, Team host, Team guest, Judge judge, Random rand)
        {
            this.date = date;
            this.host = host;
            this.guest = guest;
            this.judge = judge;
            this.goals = new List<Goal>();
            GenerateResult(rand);
        }

        public void GenerateResult(Random rand)
        {            
            guestResult = rand.Next(0, 4);
            hostResult = rand.Next(0, 6);

            for (int i = 0; i < guestResult; i++)
                goals.Add(new Goal(guest.GetRandom(rand), rand.Next(1, 92)));

            for (int i = 0; i < hostResult; i++)
                goals.Add(new Goal(host.GetRandom(rand), rand.Next(1, 92)));
        }


        public string Write()
        {
            return this.Date + "\t" + judge.Id + "\t" + host.Id + "\t" + guest.Id + "\t Wynik: " + hostResult + ":" + guestResult;

        }

        public void Save()
        {
            using (OracleConnection connection = new OracleConnection(Const.OracleConnectionString))
            {
                connection.Open();
                string saveMatch = "insert into MATCHES (MATCH_DATE, HOST_ID, GUEST_ID, JUDGE_ID) values (" +
                    "TO_DATE('" + this.date.ToString("yyyy/MM/dd") + "', 'yyyy/mm/dd')," +
                    "'" + this.host.Id + "'," +
                    "'" + this.guest.Id + "'," +
                    "'" + this.judge.Id + "')";

                Console.WriteLine(saveMatch);

                using (OracleCommand command = new OracleCommand(saveMatch, connection))
                {
                    command.ExecuteNonQuery();
                }

                string getId = "select ID from MATCHES where " +
                    "MATCH_DATE=TO_DATE('" + this.date.ToString("yyyy/MM/dd") + "','yyyy/mm/dd') and " +
                    "HOST_ID = '" + this.host.Id + "' and " +
                    "GUEST_ID='" + this.guest.Id + "' and " +
                    "JUDGE_ID='" + this.judge.Id + "'";

                int id;

                using (OracleCommand command = new OracleCommand(getId, connection))
                {
                    OracleDataReader rdr = command.ExecuteReader();
                    rdr.Read();
                    id = Int32.Parse(rdr["ID"].ToString());
                }

                foreach (var goal in this.goals)
                {
                    string saveGoal = "insert into GOALS (PLAYER_ID, MATCH_ID, GOAL_MINUTE) values (" +
                        "'"+ goal.Player.Id +"', " +
                        "'"+ id +"', " +
                        "'"+ goal.Minute +"')";

                    using (OracleCommand command = new OracleCommand(saveGoal, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }//using (OracleConnection connection = new OracleConnection(Const.OracleConnectionString))
        } // public void Save()

    }
}
