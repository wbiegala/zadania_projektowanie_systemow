﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Player
    {
        int id;
        string firstName, lastName;

        public int Id { get => id; }
        public string FirstName { get => firstName; }
        public string LastName { get => lastName; }

        public Player(int id, string firstName, string LastName)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = LastName;
        }


    }
}
