﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Teams
    {
        List<Team> teams;
        public int Count { get => this.teams.Count; }

        public Teams()
        {
            teams = new List<Team>();


            using (OracleConnection connection = new OracleConnection(Const.OracleConnectionString))
            {
                connection.Open();
                string SQL = "select ID, NAME from TEAMS";

                using (OracleCommand command = new OracleCommand(SQL, connection))
                {
                    OracleDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        int id = Int32.Parse(rdr["ID"].ToString());

                        this.teams.Add(new Team(
                            Int32.Parse(rdr["ID"].ToString()),
                            rdr["NAME"].ToString()));
                    }
                }
            }
        }

        public Team GetRandom(Random rand)
        {
            int index = rand.Next(this.teams.Count);
            return this.teams[index];
        }



    }
}
