﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generator_danych
{
    class Goal
    {
        Player player;
        int minute;

        public Goal(Player player, int minute)
        {
            this.player = player;
            this.minute = minute;
        }

        public int Minute { get => minute; }
        internal Player Player { get => player; }
    }
}
