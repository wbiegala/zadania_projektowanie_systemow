-- zadanie 5
-- Lista 3 pierwszych królów strzelców w roku 2016.

select rownum as miejsce, osoba from (
select (p.FIRST_NAME || ' ' || LAST_NAME) as osoba
from PLAYERS p
inner join GOALS g on p.ID = g.PLAYER_ID
inner join MATCHES m on g.match_id = m.id
where m.match_date between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd')
group by (p.FIRST_NAME || ' ' || LAST_NAME)
order by count(g.ID) desc)
where rownum < 4;