-- zadanie 2
-- Zestawienie zespołów oraz liczby granych przez nie meczy w I półroczu 2016 posortowane wg. liczby meczy malejąco.

select t.name as "nazwa zespolu", count(m.ID) as "ilosc granych meczy" from teams t
inner join matches m on t.ID = m.HOST_ID or t.ID = GUEST_ID
where m.match_date between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/06/30', 'yyyy/mm/dd')
group by t.name
order by count(m.ID) desc;