-- zadanie 4
-- Lista zespołów, które wygrały wszystkie mecze w roku 2016.

select TEAMS.NAME from (
select t.ID as TEAM_ID, count(*) as MATCHES_COUNT from TEAMS t
inner join MATCHES m on t.ID = m.GUEST_ID OR t.ID = m.HOST_ID
where m.MATCH_DATE between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd')
group by t.ID)
inner join 
(
select WINNER_ID, count(*) as WON_COUNT from (
select (case when mr.HOST_GOALS > mr.GUEST_GOALS THEN m.HOST_ID else (case when mr.HOST_GOALS < mr.GUEST_GOALS THEN m.GUEST_ID end) end) as WINNER_ID
from MATCHES m
inner join MATCHES_RESULTS mr on m.ID = mr.ID
where m.MATCH_DATE between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd'))
where WINNER_ID is not null
group by WINNER_ID
) on TEAM_ID = WINNER_ID
inner join TEAMS on TEAM_ID = TEAMS.ID
where MATCHES_COUNT = WON_COUNT;