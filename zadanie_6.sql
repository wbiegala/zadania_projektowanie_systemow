-- zadanie 6
-- Który sędzia sędziował największą liczbę meczy w roku 2016?

select * from(
select (j.FIRST_NAME || ' ' || j.LAST_NAME) as sedzia
from JUDGES j 
inner join MATCHES m ON j.ID=m.JUDGE_ID
where m.MATCH_DATE between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd')
group by (j.FIRST_NAME || ' ' || j.LAST_NAME)
order by count(m.ID) desc)
where rownum = 1;