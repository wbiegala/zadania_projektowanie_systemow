-- zadanie 1
-- Liczba meczy rozegranych w poszczególnych miesiącach roku 2016.

select EXTRACT(month from m.match_date) as "miesiac", count(*) as "ilosc meczy" from matches m
where m.match_date between TO_DATE ('2016/01/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd')
group by EXTRACT(month from m.match_date)
order by "miesiac";