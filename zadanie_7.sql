-- zadanie 7
-- Zestawienie zespołów oraz liczby strzelonych przez nie goli w II półroczu 2016 posortowane wg. liczby goli rosnąco. Uwzględnić tylko te zespoły, które strzeliły co najmniej 3 gole.

select t.NAME as "NAZWA ZESPOŁU", count(*) as GOLE
from GOALS g
inner join PLAYERS p on g.PLAYER_ID = p.ID
inner join MATCHES m on g.MATCH_ID = m.ID
inner join TEAMS t on p.TEAM_ID = t.ID
where m.MATCH_DATE between TO_DATE ('2016/07/01', 'yyyy/mm/dd') and TO_DATE ('2016/12/31', 'yyyy/mm/dd')
group by t.NAME
having count(*) > 3
order by GOLE asc;